<?php
  session_start();
  require_once("util.php");  

  $_POST["escuela_nombre"] = htmlspecialchars($_POST["escuela_nombre"]);

  if(isset($_POST["escuela_nombre"])) {
      if (insertarEscuela($_POST["escuela_nombre"])) {
          $_SESSION["mensaje"] = "Se agrego una nueva escuela";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al agregar una nueva  escuela";
      }
  }

  header("location:consultaEscuela.php");
?>