<?php
  session_start();
  require_once("util.php");  

  $_POST["beneficiaria"] = htmlspecialchars($_POST["beneficiaria"]);
  $_POST["Escuela"] = htmlspecialchars($_POST["Escuela"]);
  $_POST["GradoEscolar"] = htmlspecialchars($_POST["GradoEscolar"]);
  $_POST["fechaInicio"] = htmlspecialchars($_POST["fechaInicio"]);
  $_POST["fechaFin"] = htmlspecialchars($_POST["fechaFin"]);
  $_POST["tutorNombre"] = htmlspecialchars($_POST["tutorNombre"]);
  $_POST["tutorEmail"] = htmlspecialchars($_POST["tutorEmail"]);
  $_POST["tutorTelefono"] = htmlspecialchars($_POST["tutorTelefono"]);

  if(isset($_POST["beneficiaria"],$_POST["Escuela"],$_POST["GradoEscolar"])) {
      if (insertarEscolaridad($_POST["beneficiaria"],$_POST["Escuela"],$_POST["GradoEscolar"],
      $_POST["tutorNombre"],$_POST["tutorTelefono"],$_POST["tutorEmail"],
      $_POST["fechaInicio"],$_POST["fechaFin"])) {
          $_SESSION["mensaje"] = "Se agrego una nueva escolaridad";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al agregar una nueva  escolaridad";
      }
  }

  header("location:consultaEscolaridad.php");
?>