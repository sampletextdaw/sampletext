SET FOREIGN_KEY_CHECKS=0;

/*Temporal drop table para testear la creacion de las tablas para la base de datos*/
DROP TABLE IF EXISTS  Beneficiaria;
DROP TABLE IF EXISTS  Canalizador;
DROP TABLE IF EXISTS  Institucion;
DROP TABLE IF EXISTS  Escuela;
DROP TABLE IF EXISTS  Escolaridad;
DROP TABLE IF EXISTS  Estado;
DROP TABLE IF EXISTS  Ciudad;
DROP TABLE IF EXISTS  Diagnostico;
DROP TABLE IF EXISTS  Area;
DROP TABLE IF EXISTS  Especialidad;
DROP TABLE IF EXISTS  Fotos;
DROP TABLE IF EXISTS  Album;
DROP TABLE IF EXISTS  DocAnexos;
DROP TABLE IF EXISTS  Discapacidad;
DROP TABLE IF EXISTS  DiscapacidadBeneficiaria;
DROP TABLE IF EXISTS  TipoDeSangre;
DROP TABLE IF EXISTS  ProgramaAtencion;
DROP TABLE IF EXISTS  ProgramaAtencionFotos;
DROP TABLE IF EXISTS  Medicamentos;
DROP TABLE IF EXISTS  Presentacion;
DROP TABLE IF EXISTS  Receta;
DROP TABLE IF EXISTS  Usuario;
DROP TABLE IF EXISTS  UsuarioRol;
DROP TABLE IF EXISTS  Rol;
DROP TABLE IF EXISTS  RolPrivilegios;
DROP TABLE IF EXISTS  Privilegios;
DROP TABLE IF EXISTS  ProgramaAtencionBeneficiaria;
DROP TABLE IF EXISTS  BeneficiariaCanalizador;
/*Termina temporal drop table para testear la creacion de las tablas para la base de datos*/
/*****###########################  Mer   #########################****/
/*Tablas Mer*/
CREATE TABLE `Beneficiaria`
(
	`idDeIngreso` int(11) not null ,
  `idTipoSangre` int(11) ,
  `idCiudad` int(11) ,
	`fechaHoraIngreso` timestamp,
	`nombre` varchar(40),
	`apellidoM` varchar(40),
	`apellidoP` varchar(40),
	/**`edad` int(5),**/
	`fechaNacimiento` DATETIME ,
	`curp` varchar(18),
	`noDeExpediente` varchar(50),
	`ingresoConHermanos` BOOLEAN,
	`motivoIngreso` varchar(5000),
	`noDisposicion` int(11),
	`consideracionesGenerales` varchar(5000)

);
CREATE TABLE `Canalizador` 
(
	`IdCanalizador` int (11) not null,
  `idInstitucion` int (11) not null,
  `nombre` varchar(40),
	`cargo` varchar(40),
	`telefono`   varchar(10),
	`correoElectronico` varchar(40),
	`tipoIdentificacion` varchar(40),
	`numeroDeIdentificacion` varchar(40)
);
CREATE TABLE `Institucion`(
	`idInstitucion` int (11) not null,
	`nombre` varchar(40) 
);
CREATE TABLE `Escuela`
(
	`idEscuela` int(11) not null,
	`nombre` varchar(40)
);
CREATE TABLE `Estado` (
	`idEstado` int(11) not null,
	`nombre`  varchar(40)
);
CREATE TABLE `Ciudad`(
    `idCiudad` int(11) not null,
    `idEstado` int(11) not null,
    `nombre`  varchar(40)
);	
CREATE TABLE `Area`
(
	`idArea` int(11) not null,
	`nombre` varchar(40) not null
);
CREATE TABLE `Especialidad`
(
	`idEspecialidad` int(11) not null,
  `idArea` int(11) not null,
	`nombre` varchar(40) not null
);
CREATE TABLE `Fotos`
(
	`idFotos` int(11) not null,
	`textAlt` varchar(40) not null,
	`urlFotos` varchar(300) not null
);
CREATE TABLE `DocAnexos`
(
	`idDocumento` int(11) not null,
	`idDeIngreso` int(11) not null ,
	`nombre` varchar(30) not null,
	`Url` varchar(30) not null
);
CREATE TABLE `Discapacidad`
(
	`idDiscapacidad` int(11) not null,
	`Nombre` varchar(30) 
);
CREATE TABLE `TipoDeSangre`
(
	`idTipoSangre` int(11) not null,
	`Nombre` varchar(20)
);
CREATE TABLE `ProgramaAtencion`
(
	`idProgramaAtencion`  int(11) not null,
    `idArea` int(11) not null,
	`fechaInicial` DATETIME not null,
	`fechaFinal` DATETIME,
	`objetivo` varchar(1500)
);
CREATE TABLE `Medicamentos`
(
	`idMedicamento` int(11) not null,
	`nombre` varchar(40) not null,
	`ingredienteActivo` varchar(40) not null,
    `idPresentacion` int(11) not null
);
CREATE TABLE `Presentacion`
(
	`idPresentacion` int(11) not null,
	`nombre` varchar(40) not null
);
/*Termina tablas Mer*/
/*Relaciones Mer*/
CREATE TABLE `Receta`
(
	`idReceta` int(11) not null,
    `idDeIngreso` int(11) not null,
    `idMedicamento` int(11) not null,
	`fechaIni` DATETIME not null,
	`fechaFin` DATETIME,
	`descripcion` varchar(300) not null,
	`dosis` varchar(50) not null
);
CREATE TABLE `ProgramaAtencionFotos`
(
    `idProgramaAtencionFotos` int(11) not null,
	`idProgramaAtencion` int(11) not null,
    `idFotos` int(11) not null,
    `fecha` DATETIME not null
);
CREATE TABLE `ProgramaAtencionBeneficiaria`
(
    `idDeIngreso` int(11) not null,
	`idProgramaAtencion` int(11) not null,
    `fechaRegistro` DATETIME not null,
    `observaciones` varchar(1000) not null,
    `motivo` varchar(1000) not null
);

CREATE TABLE `DiscapacidadBeneficiaria`
(
    `idDiscapacidadBeneficiaria` int(11) not null,
	`idDiscapacidad` int(11) not null,
    `idDeIngreso` int(11) not null,
    `fecha` DATETIME not null,
    `curado` BOOLEAN
);
CREATE TABLE `Album`
(
	`idAlbum` int(11) not null,
	`idFotos` int(11) not null,
	`idDeIngreso` int(11) not null,
	`Nombre` varchar(30) not null
);
CREATE TABLE `Diagnostico`
(
	`idDiagnostico` int(11) not null,
	`idDeIngreso` int(11) not null ,
  `idEspecialidad` int(11) not null,
	`fecha` DATETIME not null,
	`tratamiento` varchar(500),
	`descripcion` varchar(500)
);
CREATE TABLE `Escolaridad`
(
	`idEscolaridad` int(11) not null,
	`idDeIngreso` int(11) not null ,
	`idEscuela` int(11) not null,
	`gradoEscolar` varChar(11) not null,
	`nombreTutor` varChar(11),
    `telefono`  varchar(10),
	`correoElectronico` varchar(40)

);
CREATE TABLE `BeneficiariaCanalizador`
(
	`idBeneficiariaCanalizador` int(11) not null,
	`idDeIngreso` int(11) not null ,
	`IdCanalizador` int(11) not null
);

/*Termina relaciones Mer*/
/*****###########################  Termina  Mer   #########################****/

/* ################################ RBAC ################################ */

CREATE TABLE `Usuario`
(
	`idUser` int(11) not null,
	`Usuario` varchar(26) not null,
	`password` varchar(26) not null,
	`nombre` varchar(100) not null,
	`created_at` timestamp NOT NULL DEFAULT current_timestamp()
);
CREATE TABLE `Rol`
(
	`idRol`   int(11) not null,
	`nombre` varchar(26) not null,
	`descripcion` varchar(100) not null,
	`created_at` timestamp NOT NULL DEFAULT current_timestamp()	
);
CREATE TABLE `Privilegios`
(
	`idPrivilegios`   int(11) not null,
	`nombre` varchar(26) not null,
	`descripcion` varchar(100) not null,
	`created_at` timestamp NOT NULL DEFAULT current_timestamp()	
);

/**Relaciones RBAC**/

CREATE TABLE `UsuarioRol`
    (
        `idUser` int(11) not null,
        `idRol`   int(11) not null,
        `idUsuarioRol`   int(11) not null,
        `created_at` timestamp NOT NULL DEFAULT current_timestamp()
    );
CREATE TABLE `RolPrivilegios`
    (
        `idRolPrivilegios` int(11) not null,	
        `idRol`   int(11) not null,
        `idPrivilegios`   int(11) not null,
        `created_at` timestamp NOT NULL DEFAULT current_timestamp()

    );
/**Termina relaciones RBAC**/
/* ################################ Termina RBAC ################################ */





/******         Crear Key para las tablas       *************/
/*##############################    MER     ##########*/
/** Tablas Mer**/
ALTER TABLE `Beneficiaria` 
  ADD PRIMARY KEY (`idDeIngreso`),
  ADD KEY `idTipoSangre` (`idTipoSangre`),
  ADD KEY `idCiudad` (`idCiudad`)
  ;
ALTER TABLE `Canalizador` 
  ADD PRIMARY KEY (`IdCanalizador`),
  ADD KEY `idInstitucion` (`idInstitucion`)
  ;
ALTER TABLE `Institucion` 
  ADD PRIMARY KEY (`idInstitucion`)
  ;

ALTER TABLE `Escuela` 
  ADD PRIMARY KEY (`idEscuela`)
  ;
ALTER TABLE `Estado` 
  ADD PRIMARY KEY (`idEstado`)
  ;
ALTER TABLE `Ciudad` 
  ADD PRIMARY KEY (`idCiudad`,`idEstado`)
  ;
ALTER TABLE `Area` 
  ADD PRIMARY KEY (`idArea`)
  ;
ALTER TABLE `Especialidad` 
  ADD PRIMARY KEY (`idEspecialidad`,`idArea`)
  ;
ALTER TABLE `Fotos` 
  ADD PRIMARY KEY (`idFotos`)
  ;
ALTER TABLE `DocAnexos` 
  ADD PRIMARY KEY (`idDocumento`),
  ADD  KEY `idDeIngreso` (`idDeIngreso`)
  ; 
ALTER TABLE `Discapacidad` 
  ADD PRIMARY KEY (`idDiscapacidad`)
  ;  
ALTER TABLE `TipoDeSangre` 
  ADD PRIMARY KEY (`idTipoSangre`)
  ; 
ALTER TABLE `ProgramaAtencion` 
  ADD PRIMARY KEY (`idProgramaAtencion`),
  ADD  KEY (`idArea`)  /*Checar con ricardo*/
  ; 

ALTER TABLE `Medicamentos` 
  ADD PRIMARY KEY (`idMedicamento`),
  ADD  KEY `idPresentacion` (`idPresentacion`)
  ;
ALTER TABLE `Presentacion` 
  ADD PRIMARY KEY (`idPresentacion`)
  ; 

/** Termina tablas Mer ***/

/** Relaciones N-N  Mer**/
ALTER TABLE `Receta` 
  ADD PRIMARY KEY (`idReceta`,`idDeIngreso`,`idMedicamento`)
  ;
ALTER TABLE `ProgramaAtencionFotos` 
  ADD PRIMARY KEY (`idProgramaAtencionFotos`,`idProgramaAtencion`,`idFotos`) /*Preguntar ricardo, es necesario idprogramaatencion*/
  ;
ALTER TABLE `ProgramaAtencionBeneficiaria`
  ADD PRIMARY KEY (`fechaRegistro`,`idProgramaAtencion`,`idDeIngreso`)
  ; 
ALTER TABLE `DiscapacidadBeneficiaria` 
  ADD PRIMARY KEY (`idDiscapacidadBeneficiaria`), 	/*Preguntar ricardo, es necesario idunica?*/
  ADD  KEY `idDeIngreso` (`idDeIngreso`),
  ADD  KEY `idDiscapacidad` (`idDiscapacidad`)
  ;
ALTER TABLE `Album` 
  ADD PRIMARY KEY (`idAlbum`,`idFotos`,`idDeIngreso`)
  ;
ALTER TABLE `Diagnostico`
  ADD PRIMARY KEY (`idDiagnostico`,`idDeIngreso`,`idEspecialidad`)
  ; 
ALTER TABLE `Escolaridad` 
  ADD PRIMARY KEY (`idEscolaridad`,`idDeIngreso`,`idEscuela`)
  ;
ALTER TABLE `BeneficiariaCanalizador` 
  ADD PRIMARY KEY (`idBeneficiariaCanalizador`,`idDeIngreso`,`IdCanalizador`)
  ;

/**Termina relaciones  Mer**/
/*##############################  Termina  MER     ##########*/
/* ################################ RBAC ################################ */
ALTER TABLE `Usuario` 
  ADD PRIMARY KEY (`idUser`)
  ;
ALTER TABLE `Rol` 
  ADD PRIMARY KEY (`idRol`)
  ;
ALTER TABLE `Privilegios` 
  ADD PRIMARY KEY (`idPrivilegios`)
  ;

/**Relaciones RBAC**/
ALTER TABLE `UsuarioRol` 
  ADD PRIMARY KEY (`idUsuarioRol`,`idUser`,`idRol`)
  ;
ALTER TABLE `RolPrivilegios` 
  ADD PRIMARY KEY (`idRolPrivilegios`,`idPrivilegios`,`idRol`)
  ;
/**Termina relaciones RBAC**/
/* ################################ Termina RBAC ################################ */


/******       Termina crear Key para las tablas       *************/



/******         Crear AUTO_INCREMENT para las tablas       *************/
/*##############################    MER     ##########*/
/**Tablas Mer**/
ALTER TABLE `Beneficiaria`  MODIFY `idDeIngreso` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Canalizador`  MODIFY `IdCanalizador` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Institucion`  MODIFY `idInstitucion` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Escuela`  MODIFY `idEscuela` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Estado`  MODIFY `idEstado` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Ciudad`  MODIFY `idCiudad` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Area`  MODIFY `idArea` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Especialidad`  MODIFY `idEspecialidad` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Fotos`  MODIFY `idFotos` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `DocAnexos`  MODIFY `idDocumento` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Discapacidad`  MODIFY `idDiscapacidad` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `TipoDeSangre`  MODIFY `idTipoSangre` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `ProgramaAtencion`  MODIFY `idProgramaAtencion` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Medicamentos`  MODIFY `idMedicamento` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Presentacion`  MODIFY `idPresentacion` int(11) NOT NULL AUTO_INCREMENT;

/** Termina Tablas Mer**/
/** Relaciones  Mer**/
ALTER TABLE `Receta`  MODIFY `idReceta` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `ProgramaAtencionFotos`  MODIFY `idProgramaAtencionFotos` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `DiscapacidadBeneficiaria`  MODIFY `idDiscapacidadBeneficiaria` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Album`  MODIFY `idAlbum` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Diagnostico`  MODIFY `idDiagnostico` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Escolaridad`  MODIFY `idEscolaridad` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `BeneficiariaCanalizador`  MODIFY `idBeneficiariaCanalizador` int(11) NOT NULL AUTO_INCREMENT;
/**Termina relaciones  Mer**/
/*##############################  Termina  MER     ##########*/
/* ################################ RBAC ################################ */
ALTER TABLE `Usuario`  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Rol`  MODIFY `idRol` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `Privilegios`  MODIFY `idPrivilegios` int(11) NOT NULL AUTO_INCREMENT;


/**Relaciones RBAC**/
ALTER TABLE `UsuarioRol`  MODIFY `idUsuarioRol` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `RolPrivilegios`  MODIFY `idRolPrivilegios` int(11) NOT NULL AUTO_INCREMENT;
/**Termina relaciones RBAC**/
/* ################################ Termina RBAC ################################ */





/******         Termina AUTO_INCREMENT para las tablas       *************/

/******         Definir FOREIGN KEYpara las tablas       *************/
/*##############################    MER     ##########*/
/** Tablas Mer**/
ALTER TABLE `Beneficiaria`
  ADD CONSTRAINT `beneficiariaTieneTipoCiudad` FOREIGN KEY (`idCiudad`) REFERENCES `Ciudad` (`idCiudad`),
  ADD CONSTRAINT `beneficiariaTieneTipoDeSangre` FOREIGN KEY (`idTipoSangre`) REFERENCES `TipoDeSangre` (`idTipoSangre`);
ALTER TABLE `Canalizador`
  ADD CONSTRAINT `canalizadorTieneInstitucion` FOREIGN KEY (`idInstitucion`) REFERENCES `Institucion` (`idInstitucion`);
ALTER TABLE `Ciudad`
  ADD CONSTRAINT `ciudadTieneEstado` FOREIGN KEY (`idEstado`) REFERENCES `Estado` (`idEstado`);
ALTER TABLE `Especialidad`
  ADD CONSTRAINT `especialidadTieneArea` FOREIGN KEY (`idArea`) REFERENCES `Area` (`idArea`);
ALTER TABLE `DocAnexos`
  ADD CONSTRAINT `docAnexosTieneBeneficiaria` FOREIGN KEY (`idDeIngreso`) REFERENCES `Beneficiaria` (`idDeIngreso`);
ALTER TABLE `Medicamentos`
  ADD CONSTRAINT `medicamentosTienePresentacion` FOREIGN KEY (`idPresentacion`) REFERENCES `Presentacion` (`idPresentacion`);

ALTER TABLE `ProgramaAtencion`
  ADD CONSTRAINT `ProgramaAtencionTieneArea` FOREIGN KEY (`idArea`) REFERENCES `Area` (`idArea`);
/**Termina tablas Mer**/
/** Relaciones  Mer**/
ALTER TABLE `Receta`
  ADD CONSTRAINT `recetaTieneBeneficiaria` FOREIGN KEY (`idDeIngreso`) REFERENCES `Beneficiaria` (`idDeIngreso`),
  ADD CONSTRAINT `recetatieneMeicamentos` FOREIGN KEY (`idMedicamento`) REFERENCES `Medicamentos` (`idMedicamento`);
ALTER TABLE `ProgramaAtencionFotos`
  ADD CONSTRAINT `programaAtencionFotosTieneFotos` FOREIGN KEY (`idFotos`) REFERENCES `Fotos` (`idFotos`),
  ADD CONSTRAINT `programaAtencionFotosTieneProgramaAtencion` FOREIGN KEY (`idProgramaAtencion`) REFERENCES `ProgramaAtencion` (`idProgramaAtencion`);
ALTER TABLE `ProgramaAtencionBeneficiaria`
  ADD CONSTRAINT `programaAtencionBeneficiariaTieneFotos` FOREIGN KEY (`idDeIngreso`) REFERENCES `Beneficiaria` (`idDeIngreso`),
  ADD CONSTRAINT `programaAtencionBeneficiariaTieneProgramaAtencion` FOREIGN KEY (`idProgramaAtencion`) REFERENCES `ProgramaAtencion` (`idProgramaAtencion`);

ALTER TABLE `DiscapacidadBeneficiaria`
  ADD CONSTRAINT `discapacidadBeneficiariaTieneBeneficiaria` FOREIGN KEY (`idDeIngreso`) REFERENCES `Beneficiaria` (`idDeIngreso`),
  ADD CONSTRAINT `discapacidadBeneficiariaTieneDiscapacidad` FOREIGN KEY (`idDiscapacidad`) REFERENCES `Discapacidad` (`idDiscapacidad`);
ALTER TABLE `Album`
  ADD CONSTRAINT `albumTieneFotos` FOREIGN KEY (`idFotos`) REFERENCES `Fotos` (`idFotos`),
  ADD CONSTRAINT `albumTieneBeneficiaria` FOREIGN KEY (`idDeIngreso`) REFERENCES `Beneficiaria` (`idDeIngreso`);
ALTER TABLE `Diagnostico`
  ADD CONSTRAINT `diagnosticoTieneEspecialidad` FOREIGN KEY (`idEspecialidad`) REFERENCES `Especialidad` (`idEspecialidad`),
  ADD CONSTRAINT `diagnosticoTieneBeneficiaria` FOREIGN KEY (`idDeIngreso`) REFERENCES `Beneficiaria` (`idDeIngreso`);
ALTER TABLE `Escolaridad`
  ADD CONSTRAINT `escolaridadTieneEscuela` FOREIGN KEY (`idEscuela`) REFERENCES `Escuela` (`idEscuela`),
  ADD CONSTRAINT `escolaridadTieneBeneficiaria` FOREIGN KEY (`idDeIngreso`) REFERENCES `Beneficiaria` (`idDeIngreso`);

ALTER TABLE `BeneficiariaCanalizador`
  ADD CONSTRAINT `beneficiariaCanalizadorTieneCanalizador` FOREIGN KEY (`IdCanalizador`) REFERENCES `Canalizador` (`IdCanalizador`),
  ADD CONSTRAINT `beneficiariaCanalizadorTieneBeneficiaria` FOREIGN KEY (`idDeIngreso`) REFERENCES `Beneficiaria` (`idDeIngreso`);

/**Termina relaciones  Mer**/
/*##############################  Termina  MER     ##########*/
/* ################################ RBAC ################################ */
/**Relaciones RBAC**/
ALTER TABLE `UsuarioRol`
  ADD CONSTRAINT `usuarioRolTieneUsuario` FOREIGN KEY (`idUser`) REFERENCES `Usuario` (`idUser`),
  ADD CONSTRAINT `usuarioRolTieneRol` FOREIGN KEY (`idRol`) REFERENCES `Rol` (`idRol`);
ALTER TABLE `RolPrivilegios`
  ADD CONSTRAINT `rolPrivilegiosTienePrivilegios` FOREIGN KEY (`idPrivilegios`) REFERENCES `Privilegios` (`idPrivilegios`),
  ADD CONSTRAINT `rolPrivilegiosTieneRol` FOREIGN KEY (`idRol`) REFERENCES `Rol` (`idRol`);

/**Termina relaciones RBAC**/
/* ################################ Termina RBAC ################################ */
/******         Termina definir FOREIGN KEYpara las tablas       *************/
SET FOREIGN_KEY_CHECKS=1;


  



/*//////////////////////////////////////////////////////////////Datos*/


/** Completa  **/
INSERT INTO `Estado` (`nombre`)
VALUE
('Aguascalientes'),
('Baja California'),
('Baja California Sur'),
('Campeche'),
('Chiapas'),
('Chihuahua'),
('Coahuila'),
('Colima'),
('Distrito Federal'),
('Durango'),
('Estado de México'),
('Guanajuato'),
('Guerrero'),
('Hidalgo'),
('Jalisco'),
('Michoacán'),
('Morelos'),
('Nayarit'),
('Nuevo León'),
('Oaxaca'),
('Puebla'),
('Querétaro'),
('Quintana Roo'),
('San Luis Potosí'),
('Sinaloa'),
('Sonora'),
('Tabasco'),
('Tamaulipas'),
('Tlaxcala'),
('Veracruz'),
('Yucatán'),
('Zacatecas');
/*cOMPLETO*/
INSERT INTO `Ciudad` (`idEstado`,`nombre`)
VALUE
(21,'Querétaro'),
(21,'El Pueblito'),
(21,'Peña de Bernal'),
(21,'San Juan del Rio'),
(21,'Tequisquiapan'),
(10,'Ciudad A'),
(10,'Ciudad B'),
(11,'Ciudad C'),
(12,'Ciudad D'),
(15,'Ciudad F'),
(16,'Ciudad G'),
(18,'Ciudad H'),
(27,'Ciudad I'),
(28,'Ciudad J'),
(11,'Ciudad K'),
(3,'Ciudad L'),
(5,'Ciudad M'),
(6,'Ciudad N'),
(7,'Ciudad O'),
(8,'Ciudad P')
;

/**Completo*/
INSERT INTO `TipoDeSangre` (`nombre`)
VALUE
('O negativo'),
('O positivo'),
('A negativo'),
('A positivo'),
('B negativo'),
('B positivo'),
('AB negativo'),
('AB positivo');


/*mínimo de 20 registros*/

INSERT INTO `Beneficiaria` (`idTipoSangre`,`idCiudad`,`fechaHoraIngreso`,`nombre`,`apellidoM`,`apellidoP`,`fechaNacimiento`,`curp`,`noDeExpediente`,`ingresoConHermanos`,`motivoIngreso`,`noDisposicion`,`consideracionesGenerales`) 
VALUES
(1,1,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(1,4,'2019-10-02 17:21:34','Aaron','Hudson','Windler','2015-10-01','AAHW770826HCLRDN13','a054504545',0,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,05454454,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(1,3,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(2,3,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(1,2,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(4,5,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(5,4,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(6,7,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(5,5,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(7,7,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(7,4,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(7,3,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(1,3,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(2,4,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(4,3,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(8,4,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(8,7,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(8,9,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(8,3,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
),
(6,1,'2019-01-12 03:55:19','Werner','Wuckert','Boyer','2017-02-11','PERM770826HCLRDR73','a05845095',1,
'Doloremque pariatur qui pariatur dignissimos distinctio sit. Ut quibusdam ipsam veniam eligendi inventore ut animi voluptatibus velit. Dolorum reprehenderit fugiat dolor quas alias numquam nobis commodi qui.'
,10541504,
'Beatae est consequuntur facilis quam voluptatum. Eum eaque facere nihil corporis non voluptatibus labore cumque labore. Odio aut maxime ut voluptatem porro sapiente a. Dignissimos molestias adipisci rem aut vel ut architecto dolorum beatae. Est iure quam aut fuga.'
)
;
INSERT INTO `Institucion` (`nombre`)
VALUE
('Independiente'),
('Dif'),
('Institucion B'),
('Institucion C'),
('Institucion D'),
('Institucion F'),
('Institucion G'),
('Institucion H'),
('Institucion I'),
('Institucion J'),
('Institucion K'),
('Institucion L'),
('Institucion M'),
('Institucion N'),
('Institucion O'),
('Institucion P'),
('Institucion Q'),
('Institucion R'),
('Institucion S'),
('Institucion T');
/**Completo*/
INSERT INTO `Escuela` (`nombre`)
VALUE
('Tec'),
('Escuela A'),
('Escuela B'),
('Escuela C'),
('Escuela D'),
('Escuela F'),
('Escuela G'),
('Escuela H'),
('Escuela I'),
('Escuela J'),
('Escuela K'),
('Escuela L'),
('Escuela M'),
('Escuela N'),
('Escuela O'),
('Escuela P'),
('Escuela Q'),
('Escuela R'),
('Escuela S'),
('Escuela T');
/*Completp**/
INSERT INTO `Canalizador` (`idInstitucion` ,`nombre` ,`cargo` ,`telefono`   ,`correoElectronico` ,`tipoIdentificacion` ,`numeroDeIdentificacion` )
VALUE
(1,'Efren OConnell','No aplica','824-852-5655','d@outlook.com','INE','A05848754154'),
(1,'Delaney Senger','Lead Mobility Facilitator','698-124-2959','ddsa@outlook.com','Pasaporte','2378437'),
(3,'Bridie Schmidt','International Accounts Director','836-366-9150','dasd@outlook.com','INE','sfagf'),
(2,'Sister Hyatt Sr.','Senior Brand Designer','917-811-2995','d@outlook.com','INE','sgfedvag'),
(9,'Melyssa Connelly','Customer Operations Executive','948-226-8638','dfse@outlook.com','Pasaporte','5424'),
(7,'Holly Bins','Lead Operations Engineer','752-117-7679','fesd@outlook.com','INE','asgd'),
(2,'Eudora Goyette','Dynamic Integration Consultant','176-110-8477','rg@outlook.com','INE','sdag'),
(11,'Ms. Nicole Kreiger','Dynamic Optimization Liaison','932-657-3566','dfgd@outlook.com','INE','asgfsa'),
(10,'Austen Gislason','Principal Usability Assistant','328-176-2265','gfdgdd@outlook.com','Pasaporte','4520'),
(10,'Al Pouros ','Senior Functionality Strategist','580-399-4678','dfgd@outlook.com','Pasaporte','54354'),
(19,'Quinton Littel','Central Accountability Orchestrator','573-193-3971','hgfd@outlook.com','INE','asdgfasg'),
(18,'Hershel Bins','National Interactions Officer','745-144-6583','htfhd@outlook.com','INE','agdfsfv'),
(1,'Wilfred Effertz','Chief Response Orchestrator','103-428-8889','fthd@outlook.com','INE','asdgfsa'),
(12,'Ali Kilback','Human Communications Planner','775-918-2253','fhxd@outlook.com','Pasaporte','4536'),
(10,'Zoie Durgan','Dynamic Creative Planner','020-697-4104','zdrhd@outlook.com','Pasaporte','4523434'),
(9,'Kenya Sanford','Direct Branding Producer','169-510-4908','drzdh@outlook.com','INE','asdfgas'),
(4,'Lilian Reichel PhD','Dynamic Assurance Orchestrator','119-623-7338','zdhrd@outlook.com','INE','fdaswf'),
(3,'Dr. Edison Mueller','Regional Implementation Administrator','804-353-5995','dzrh@outlook.com','Pasaporte','safsdaasfd'),
(1,'Emory Champlin','Central Implementation Engineer','974-095-3996','dzrh@outlook.com','INE','afsdasf'),
(2,'Pasquale Towne','Future Factors Facilitator','164-330-6477','hrtd@outlook.com','INE','gersagfasg');
 /**Completado*/
INSERT INTO `Area` (`nombre`)
VALUE
('Medcina'),
('Psicologia'),
('Educacion'),
('Area C'),
('Area D'),
('Area F'),
('Area G'),
('Area H'),
('Area I'),
('Area J'),
('Area K'),
('Area L'),
('Area M'),
('Area N'),
('Area O'),
('Area P'),
('Area Q'),
('Area R'),
('Area S'),
('Area T');

/**Completado**/
INSERT INTO `Especialidad` (`idArea`,`nombre`)
VALUE
(1,'Medcina General'),
(1,'Psicologia General'),
(2,'Educacion General'),
(4,'Especialidad C'),
(1,'Especialidad D'),
(2,'Especialidad F'),
(3,'Especialidad G'),
(2,'Especialidad H'),
(1,'Especialidad I'),
(6,'Especialidad J'),
(8,'Especialidad K'),
(9,'Especialidad L'),
(12,'Especialidad M'),
(15,'Especialidad N'),
(18,'Especialidad O'),
(12,'Especialidad P'),
(19,'Especialidad Q'),
(2,'Especialidad R'),
(1,'Especialidad S'),
(3,'Especialidad T');
/**Completo*/
INSERT INTO `Fotos` (`textAlt`,`urlFotos`)
VALUE
('Foto','http://lorempixel.com/640/480/abstract'),
('Fotos A','https://s3.amazonaws.com/uifaces/faces/twitter/anatolinicolae/128.jpg'),
('Fotos B','http://lorempixel.com/640/480/sports'),
('Fotos C','http://lorempixel.com/640/480/people'),
('Fotos D','http://lorempixel.com/640/480/people'),
('Fotos F','http://lorempixel.com/640/480/animals'),
('Fotos G','http://lorempixel.com/640/480/animals'),
('Fotos H','http://lorempixel.com/640/480/nightlife'),
('Fotos I','http://lorempixel.com/640/480/transport'),
('Fotos J','http://lorempixel.com/640/480/food'),
('Fotos K','http://lorempixel.com/640/480/nature'),
('Fotos L','http://lorempixel.com/640/480/sports'),
('Fotos M','http://lorempixel.com/640/480/cats'),
('Fotos N','http://lorempixel.com/640/480/transport'),
('Fotos O','http://lorempixel.com/640/480/technics'),
('Fotos P','http://lorempixel.com/640/480/transport'),
('Fotos Q','http://lorempixel.com/640/480/sports'),
('Fotos R','http://lorempixel.com/640/480/abstract'),
('Fotos S','http://lorempixel.com/640/480/business'),
('Fotos T','https://s3.amazonaws.com/uifaces/faces/twitter/bryan_topham/128.jpg');
/**Completo**/
INSERT INTO `Discapacidad` (`nombre`)
VALUE
('Monoplejia'),
('Parálisis cerebral'),
('Amputación'),
('Discapacidad C'),
('Discapacidad D'),
('Discapacidad F'),
('Discapacidad G'),
('Discapacidad H'),
('Discapacidad I'),
('Discapacidad J'),
('Discapacidad K'),
('Discapacidad L'),
('Discapacidad M'),
('Discapacidad N'),
('Discapacidad O'),
('Discapacidad P'),
('Discapacidad Q'),
('Discapacidad R'),
('Discapacidad S'),
('Discapacidad T');

/**Completo**/
INSERT INTO `Presentacion` (`nombre`)
VALUE
('Capsulas'),
('Pasta'),
('Jarabes'),
('Presentacion C'),
('Presentacion D'),
('Presentacion F'),
('Presentacion G'),
('Presentacion H'),
('Presentacion I'),
('Presentacion J'),
('Presentacion K'),
('Presentacion L'),
('Presentacion M'),
('Presentacion N'),
('Presentacion O'),
('Presentacion P'),
('Presentacion Q'),
('Presentacion R'),
('Presentacion S'),
('Presentacion T');


/**Completado**/
INSERT INTO `Medicamentos` (`idPresentacion`,`nombre`,`ingredienteActivo`)
VALUE
(1,'Adderal','Salbutamol'),
(1,'Dexedrina','Azitromicina'),
(2,'Focalin','Mitazapina'),
(4,'Medicamentos C','Ingrediente Activo A'),
(1,'Medicamentos D','Ingrediente Activo B'),
(2,'Medicamentos F','Ingrediente Activo C'),
(3,'Medicamentos G','Ingrediente Activo D'),
(2,'Medicamentos H','Ingrediente Activo E'),
(1,'Medicamentos I','Ingrediente Activo F'),
(6,'Medicamentos J','Ingrediente Activo G'),
(8,'Medicamentos K','Ingrediente Activo H'),
(9,'Medicamentos L','Ingrediente Activo I'),
(12,'Medicamentos M','Ingrediente Activo J'),
(15,'Medicamentos N','Ingrediente Activo K'),
(18,'Medicamentos O','Ingrediente Activo L'),
(12,'Medicamentos P','Ingrediente Activo M'),
(19,'Medicamentos Q','Ingrediente Activo N'),
(2,'Medicamentos R','Ingrediente Activo O'),
(1,'Medicamentos S','Ingrediente Activo P'),
(3,'Medicamentos T','Ingrediente Activo Q');

INSERT INTO `ProgramaAtencion` (`idArea`,`fechaInicial`,`fechaFinal`,`objetivo`)
VALUE
(3,'2020-02-04','2020-08-06',
'Nam voluptate maxime asperiores optio occaecati amet sequi ut. Fugiat quae quas maxime enim quas ut. Necessitatibus molestiae pariatur ratione expedita sit qui. Mollitia qui est dolorem dicta impedit.
Consequatur amet in rem et molestiae molestiae pariatur totam. Distinctio cum magnam nisi rem mollitia eius. Et vero aut ducimus ex ea cumque consequuntur mollitia. Cupiditate sit veniam omnis vel et voluptatem. Perferendis voluptas tempora molestiae alias ipsum ipsa. Quia et aperiam provident et.
Aperiam voluptatibus illum id sunt. Sint nulla alias modi minima tempore quae voluptas aut magni. Qui repellendus similique. Facere aperiam magni molestiae. Aut sint ea eos sed et. Aspernatur eaque quis quo consequatur voluptate adipisci officia iusto minima.'
),
(1,'2020-04-09','2020-08-09','Nobis et adipisci rerum aspernatur accusamus cupiditate deserunt eum est. Expedita voluptatem enim laudantium eligendi et error. Aliquid ut sit voluptates consectetur eos beatae. Reiciendis veritatis repudiandae voluptatem nihil nesciunt provident eaque. Id tempore qui quia rem. A doloremque aliquam dolor sint aut aut praesentium voluptatem autem.'),
(2,'2019-05-19','2022-03-11','Tempora sed accusamus possimus eos reiciendis soluta. Nam perspiciatis nostrum dignissimos velit pariatur est. Consequatur blanditiis corrupti sit aut cumque non est. Facere ut hic consectetur qui qui fugit veritatis aut. Omnis ratione inventore similique vero. Enim et eum ut aut nemo illum sunt et sint.
 
Qui et voluptatem animi. Nam a ex rerum tenetur illum ut itaque voluptatem qui. Et sapiente itaque blanditiis tempora aut.
 
Facilis non doloribus rerum. Sed dignissimos libero temporibus laudantium nesciunt asperiores veniam nam. Non omnis numquam odit dolores ut delectus ut magni recusandae. Maiores ducimus voluptatem harum quidem consectetur numquam eaque dolores saepe. Unde esse velit sint velit ut sint quo.'),
(4,'2019-12-08','2020-10-11','Aut mollitia modi omnis sed. Saepe consectetur et sed voluptatum. Sint magni provident voluptatem enim consequatur enim officia sit. Cum nihil itaque rem eveniet commodi ducimus.'),
(1,'2019-12-11','2023-12-11','Repudiandae aut aspernatur.
Cupiditate dolorum omnis delectus a est fuga placeat illo sunt.'),
(2,'2019-11-17','2022-11-11','Temporibus dignissimos natus est quae. Dolorem laudantium iste quibusdam labore alias corporis deleniti vitae. Perspiciatis id alias enim rerum est.'),
(3,'2019-08-13','2023-5-11','nihil vero voluptatem'),
(2,'2019-10-08','2021-06-11','laboriosam velit ad'),
(1,'2019-06-22','2020-08-11','Quaerat cupiditate non praesentium hic ut est incidunt porro.'),
(6,'2019-08-10','2022-09-11','Placeat aut eaque. In beatae aliquam molestiae nulla. Ut natus voluptas quo ducimus. Doloribus numquam laborum dolor ad provident tempore rerum unde. Sit soluta et.'),
(8,'2020-02-21','2022-12-11','Qui totam facere quos doloribus.'),
(9,'2019-09-18','2020-11-11','Laborum iste placeat quia ut odio fugit dicta.
Facilis perspiciatis quia.'),
(12,'2020-02-25','2021-1-11','expedita blanditiis perspiciatis'),
(15,'2019-09-11','2022-3-11','quae-nihil-distinctio'),
(18,'2018-04-13','2020-7-11','Ea in distinctio quia omnis ad. Sint magni amet aliquam. Doloremque tempore culpa adipisci aut et non reprehenderit molestiae distinctio.'),
(12,'2017-05-15','2019-8-11','Excepturi quae et tenetur ex eos. Qui accusantium ea. Sequi mollitia dolor eaque et cupiditate vitae autem et enim.'),
(19,'2015-11-1','2017-9-11','Vel provident exercitationem sed qui dolorem molestias pariatur. Quis aut quis et eius ratione. In necessitatibus maxime ea ullam aliquam dolores unde ipsa.
 
Delectus eveniet recusandae error tempora nihil beatae minus non. Saepe debitis cum. Doloribus dolor voluptatibus. Quis omnis ipsa laborum dolorum aspernatur explicabo. Et provident et ut architecto rerum tempora ut tempore.
 
Sunt ut quibusdam autem in cupiditate. Voluptate labore quas temporibus voluptatibus repudiandae vel tenetur. Dicta qui cupiditate omnis impedit in excepturi. Inventore dolores est est et quo quia reprehenderit excepturi laudantium.'),
(2,'2014-10-2','2015-6-11','Harum et nihil. Voluptas et et veniam ea. Eius ea laudantium illum et earum.'),
(1,'2010-10-5','2011-12-11','Eos odit est quia dignissimos eum aliquid placeat.'),
(3,'2013-05-8','2013-9-11','quia');


INSERT INTO `DocAnexos` (`idDeIngreso`,`Url`)
VALUE
(1,'http://lorempixel.com/640/480/abstract'),
(1,'https://s3.amazonaws.com/uifaces/faces/twitter/anatolinicolae/128.jpg'),
(2,'http://lorempixel.com/640/480/sports'),
(3,'http://lorempixel.com/640/480/people'),
(5,'http://lorempixel.com/640/480/people'),
(10,'http://lorempixel.com/640/480/animals'),
(12,'http://lorempixel.com/640/480/animals'),
(19,'http://lorempixel.com/640/480/nightlife'),
(1,'http://lorempixel.com/640/480/transport'),
(1,'http://lorempixel.com/640/480/food'),
(1,'http://lorempixel.com/640/480/nature'),
(2,'http://lorempixel.com/640/480/sports'),
(10,'http://lorempixel.com/640/480/cats'),
(11,'http://lorempixel.com/640/480/transport'),
(1,'http://lorempixel.com/640/480/technics'),
(17,'http://lorempixel.com/640/480/transport'),
(18,'http://lorempixel.com/640/480/sports'),
(5,'http://lorempixel.com/640/480/abstract'),
(7,'http://lorempixel.com/640/480/business'),
(9,'https://s3.amazonaws.com/uifaces/faces/twitter/bryan_topham/128.jpg');
INSERT INTO`Receta`(`idDeIngreso`,`idMedicamento`,`fechaIni`,`fechaFin`,`descripcion`,`dosis`)
VALUE
(1,2,'2019-3-15','2019-4-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(2,1,'2018-2-15','2018-3-25','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(3,3,'2017-1-15','2017-2-25','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(5,5,'2016-12-15','2016-12-17','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(7,10,'2015-08-15','2015-10-19','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(8,12,'2014-3-15','2014-4-20','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(10,19,'2012-4-15','2012-5-25','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(11,18,'2014-5-15','2014-7-10','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(13,11,'2015-6-15','2015-6-11','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(15,10,'2017-7-15','2017-7-16','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(5,9,'2018-8-15','2018-8-17','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(10,8,'2020-9-15','2020-9-18','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(19,7,'2020-10-15','2020-10-19','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(18,3,'2020-11-15','2020-11-20','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(17,2,'2020-12-15','2020-12-21','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(16,1,'2020-1-15','2020-1-22','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(15,6,'2020-2-15','2020-2-23','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(7,10,'2020-3-15','2020-3-24','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(14,11,'2020-4-15','2020-4-25','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(13,12,'2011-5-15','2011-5-26','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(11,15,'2018-6-15','2018-6-27','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(1,17,'2010-7-15','2010-7-28','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(3,19,'2015-8-15','2015-8-29','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(2,18,'2016-9-15','2016-9-30','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(4,12,'2017-1-15','2017-1-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(5,17,'2018-10-15','2018-10-29','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(6,3,'2019-11-15','2019-11-28','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(7,5,'2017-12-15','2017-12-27','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(1,7,'2015-1-15','2015-1-26','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(2,7,'2016-01-15','2016-01-25','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(3,8,'2014-3-15','2014-3-24','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(15,3,'2013-3-15','2013-3-23','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(17,9,'2012-3-15','2012-3-22','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(18,9,'2011-3-15','2011-3-20','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(19,10,'2010-5-15','2010-5-19','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(3,11,'2015-7-15','2015-9-18','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(4,19,'2001-8-15','2001-10-16','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(5,7,'2008-9-15','2008-11-17','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(6,9,'2007-10-15','2007-10-18','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(2,8,'2005-11-15','2005-11-19','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(3,2,'2006-12-15','2006-12-20','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(5,2,'2007-3-15','2007-3-21','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(7,2,'2008-2-15','2008-2-22','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(8,1,'2009-5-15','2009-7-23','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(9,3,'2010-7-15','2010-9-24','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(11,5,'2011-9-15','2011-7-25','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(10,6,'2012-7-15','2012-8-25','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(18,6,'2013-8-15','2013-9-25','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(14,8,'2015-6-15','2015-6-25','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(13,8,'2017-5-15','2017-5-25','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.');



INSERT INTO`ProgramaAtencionFotos`(`idProgramaAtencion`,`idFotos`,`fecha`)
VALUE
(1,2,'2019-3-15'),
(2,1,'2018-2-15'),
(3,3,'2017-1-15'),
(5,5,'2016-12-15'),
(7,10,'2015-08-15'),
(8,12,'2014-3-15'),
(10,19,'2012-4-15'),
(11,18,'2014-5-15'),
(13,11,'2015-6-15'),
(15,10,'2017-7-15'),
(5,9,'2018-8-15'),
(10,8,'2020-9-15'),
(19,7,'2020-10-15'),
(18,3,'2020-11-15'),
(17,2,'2020-12-15'),
(16,1,'2020-1-15'),
(15,6,'2020-2-15'),
(7,10,'2020-3-15'),
(14,11,'2020-4-15'),
(13,12,'2011-5-15'),
(11,15,'2018-6-15'),
(1,17,'2010-7-15'),
(3,19,'2015-8-15'),
(2,18,'2016-9-15'),
(4,12,'2017-1-15'),
(5,17,'2018-10-15'),
(6,3,'2019-11-15'),
(7,5,'2017-12-15'),
(1,7,'2015-1-15'),
(2,7,'2016-01-15'),
(3,8,'2014-3-15'),
(15,20,'2013-3-15'),
(17,9,'2012-3-15'),
(18,9,'2011-3-15'),
(19,10,'2010-5-15'),
(3,11,'2015-7-15'),
(5,19,'2001-8-15'),
(7,7,'2008-9-15'),
(5,9,'2007-10-15'),
(2,8,'2005-11-15'),
(3,2,'2006-12-15'),
(5,2,'2007-3-15'),
(7,2,'2008-2-15'),
(8,1,'2009-5-15'),
(9,3,'2010-7-15'),
(11,5,'2011-9-15'),
(10,6,'2012-7-15'),
(18,6,'2013-8-15'),
(14,8,'2015-6-15'),
(13,8,'2017-5-15');


INSERT INTO`ProgramaAtencionBeneficiaria`(`idDeIngreso`,`idProgramaAtencion`,`fechaRegistro`,`observaciones`,`motivo`)
VALUE
(1,1,'2019-3-15 00:00:00','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(1,2,'2018-2-15  00:00:01','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(2,3,'2017-1-15  00:00:02','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(3,5,'2016-12-15  00:00:03','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(4,7,'2015-08-15  00:00:04','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(5,8,'2014-3-15  00:00:05','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(6,10,'2012-4-15  00:00:06','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(7,11,'2014-5-15  00:00:07','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(8,13,'2015-6-15  00:00:08','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(9,15,'2017-7-15  00:00:09','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(10,5,'2018-8-15  00:00:10','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(11,10,'2020-9-15 00:00:11','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(12,19,'2020-10-15 00:00:12','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(13,18,'2020-11-15 00:00:13','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(14,17,'2020-12-15 00:00:14','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(15,16,'2020-1-15 00:00:15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(16,15,'2020-2-15 00:00:16','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(17,7,'2020-3-15 00:00:17','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(18,14,'2020-4-15 00:00:18','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(19,13,'2011-5-15 00:00:19','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(19,11,'2018-6-15 00:00:20','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(1,1,'2010-7-15 00:00:21','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(2,3,'2015-8-15 00:00:22','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(3,2,'2016-9-15 00:00:23','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(4,4,'2017-1-15 00:00:24','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(5,5,'2018-10-15 00:00:25','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(6,6,'2019-11-15 00:00:26','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(7,7,'2017-12-15 00:00:27','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(8,1,'2015-1-15 00:00:28','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(9,2,'2016-01-15 00:00:29','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(5,3,'2014-3-15 00:00:30','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(11,15,'2013-3-15 00:00:31','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(12,17,'2012-3-15 00:00:32','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(13,18,'2011-3-15  00:00:33','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(14,19,'2010-5-15  00:00:34','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(15,20,'2015-7-15  00:00:35','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(16,19,'2001-8-15  00:00:36','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(17,20,'2008-9-15  00:00:37','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(18,20,'2007-10-15  00:00:38','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(19,2,'2005-11-15  00:00:39','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(8,3,'2006-12-15  00:00:40','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(1,5,'2007-3-15  00:00:41','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(2,7,'2008-2-15  00:00:42','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(3,8,'2009-5-15  00:00:43','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(4,9,'2010-7-15  00:00:44','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(5,11,'2011-9-15  00:00:45','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(6,10,'2012-7-15  00:00:46','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(7,18,'2013-8-15  00:00:47','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(8,14,'2015-6-15  00:00:48','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(9,13,'2017-5-15  00:00:49','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.');


INSERT INTO`DiscapacidadBeneficiaria`(`idDiscapacidad`,`idDeIngreso`,`fecha`,`curado`)
VALUE
(1,2,'2019-3-15',1),
(2,1,'2018-2-15',0),
(3,3,'2017-1-15',1),
(5,5,'2016-12-15',0),
(7,10,'2015-08-15',0),
(8,12,'2014-3-15',1),
(10,19,'2012-4-15',1),
(11,18,'2014-5-15',0),
(13,11,'2015-6-15',0),
(15,10,'2017-7-15',1),
(5,9,'2018-8-15',1),
(10,8,'2020-9-15',0),
(19,7,'2020-10-15',1),
(18,3,'2020-11-15',0),
(17,2,'2020-12-15',1),
(16,1,'2020-1-15',1),
(15,6,'2020-2-15',0),
(7,10,'2020-3-15',0),
(14,11,'2020-4-15',0),
(13,12,'2011-5-15',1),
(11,15,'2018-6-15',1),
(1,17,'2010-7-15',1),
(3,19,'2015-8-15',1),
(2,18,'2016-9-15',0),
(4,12,'2017-1-15',0),
(5,17,'2018-10-15',1),
(6,3,'2019-11-15',1),
(7,5,'2017-12-15',0),
(1,7,'2015-1-15',0),
(2,7,'2016-01-15',0),
(3,8,'2014-3-15',0),
(15,20,'2013-3-15',1),
(17,9,'2012-3-15',1),
(18,9,'2011-3-15',1),
(19,10,'2010-5-15',0),
(20,11,'2015-7-15',1),
(20,19,'2001-8-15',0),
(20,7,'2008-9-15',1),
(20,9,'2007-10-15',0),
(2,8,'2005-11-15',0),
(3,2,'2006-12-15',1),
(5,2,'2007-3-15',1),
(7,2,'2008-2-15',1),
(8,1,'2009-5-15',0),
(9,3,'2010-7-15',0),
(11,5,'2011-9-15',0),
(10,6,'2012-7-15',1),
(18,6,'2013-8-15',1),
(14,8,'2015-6-15',1),
(13,8,'2017-5-15',0);

INSERT INTO`Album`(`idFotos`,`idDeIngreso`,`Nombre`)
VALUE
(1,2,'vel'),
(2,1,'consequuntur sequi quae'),
(3,3,'quaerat-ea-at'),
(5,5,'quod'),
(7,10,'tenetur voluptas quos'),
(8,12,'ut-et-sit'),
(10,19,'at deleniti at'),
(11,18,'eos'),
(13,11,'facere'),
(15,10,'esse et qui'),
(5,9,'atque aut et'),
(10,8,'ea'),
(19,7,'maiores-nisi-occaecati'),
(18,3,'et'),
(17,2,'sit ea quia'),
(16,1,'soluta-qui-sit'),
(15,6,'deserunt-dolor-accusamus'),
(7,10,'et-est-omnis'),
(14,11,'in'),
(13,12,'quos'),
(11,15,'veniam'),
(1,17,'voluptas'),
(3,19,'quibusdam eum enim'),
(2,18,'fugit tempore magnam'),
(4,12,'natus soluta sed'),
(5,17,'sint-dolor-reprehenderit'),
(6,3,'provident repellat fugit'),
(7,5,'natus'),
(1,7,'Odio optio nihil distinctio.'),
(2,7,'et'),
(3,8,'aut'),
(15,3,'sequi'),
(17,9,'ex'),
(18,9,'cumque omnis enim'),
(19,10,'quidem-alias-unde'),
(1,11,'eos'),
(2,19,'hic'),
(3,7,'odit'),
(4,9,'blanditiis'),
(2,8,'quo impedit reprehenderit'),
(3,2,'fuga-ex-quos'),
(5,2,'soluta-ea-ad'),
(7,2,'officiis'),
(8,1,'eum'),
(11,5,'totam non sint'),
(10,6,'Inventore aliquid et dolores sed dicta.'),
(18,6,'perspiciatis harum eos'),
(14,8,'aperiam'),
(13,8,'odit');

INSERT INTO`Diagnostico`(`idDeIngreso`,`idEspecialidad`,`fecha`,`tratamiento`,`descripcion`)
VALUE
(1,2,'2019-3-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(2,1,'2018-2-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(3,3,'2017-1-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(5,5,'2016-12-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(7,10,'2015-08-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(8,12,'2014-3-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(10,19,'2012-4-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(11,18,'2014-5-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(13,11,'2015-6-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(15,10,'2017-7-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(5,9,'2018-8-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(10,8,'2020-9-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(19,7,'2020-10-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(18,3,'2020-11-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(17,2,'2020-12-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(16,1,'2020-1-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(15,6,'2020-2-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(7,10,'2020-3-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(14,11,'2020-4-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(13,12,'2011-5-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(11,15,'2018-6-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(1,17,'2010-7-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(3,19,'2015-8-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(2,18,'2016-9-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(4,12,'2017-1-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(5,17,'2018-10-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(6,3,'2019-11-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(7,5,'2017-12-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(1,7,'2015-1-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(2,7,'2016-01-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(3,8,'2014-3-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(15,5,'2013-3-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(17,9,'2012-3-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(18,9,'2011-3-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(19,10,'2010-5-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(20,11,'2015-7-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(20,19,'2001-8-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(20,7,'2008-9-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(14,9,'2007-10-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(2,8,'2005-11-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(3,2,'2006-12-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(5,2,'2007-3-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(7,2,'2008-2-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(8,1,'2009-5-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(9,3,'2010-7-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(11,5,'2011-9-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(10,6,'2012-7-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(18,6,'2013-8-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(14,8,'2015-6-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.'),
(13,8,'2017-5-15','Sit repellendus temporibus eos soluta qui eos nesciunt harum voluptas.','Non eum hic quae fugiat dolorem.');


INSERT INTO`Escolaridad`(`idDeIngreso`,`idEscuela`,`gradoEscolar`,`nombreTutor`,`telefono`,`correoElectronico`)
VALUE
(1,2,'1 Primaria','Lavinia Daugherty','973-747-1906','asd@outlook.com'),
(2,1,'3 Primaria','Minnie Abbott','214-159-4234','ragf@gmail.com'),
(3,3,'5 Primaria','Miss Catharine Mosciski','108-715-5260','agr@outlook.com'),
(5,5,'1 Secundaria','Danial Okuneva','636-064-5430','sdfAF@outlook.com'),
(7,10,'5 Primaria','Quinten Muller','692-096-6683','feawf@gmail.com'),
(8,12,'3 Primaria','Cornell Cummerata','608-052-2200','afew@hotmail..com'),
(10,19,'1 Secundaria','Jennyfer Graham','225-985-6902','afwef@outlook.com'),
(11,18,'5 Primaria','Halle Johnston','270-454-7040','dthyur@gmail.com'),
(13,11,'1 Primaria','Garret Zboncak','701-745-4612','krfyu@gmail.com'),
(15,10,'1 Secundaria','Danyka Kuphal IV','886-381-7566','qfrf@gmail.com'),
(5,9,'5 Primaria','Una Mayert V','028-005-1612','rfg@outlook.com'),
(10,8,'3 Primaria','Lauren Schuppe','003-879-6372','dgstr@outlook.com'),
(19,7,'3 Secundaria','Augustine Collier','743-683-6287','67tru@hotmail..com'),
(18,3,'4 Primaria','Zula Hyatt','183-139-8719','@gmail.com'),
(17,2,'5 Primaria','Clementina Thiel','327-619-0051','yuikjyfht@gmail.com'),
(16,1,'4 Primaria','Natalie Halvorson','246-925-7021','sgrfd@outlook.com'),
(15,6,'1 Primaria','Sophie Stiedemann','215-709-3600','ertgyh@outlook.com'),
(7,10,'1 Secundaria','Miss Kaelyn Howell','919-983-2211','ytr@gmail.com'),
(14,11,'5 Primaria','Jalyn Deckow','648-141-1326','fse@hotmail..com'),
(13,12,'3 Secundaria','Antonina Quitzon','436-383-1262','8iujh@outlook.com'),
(11,15,'3 Primaria','Vita Schneider','317-251-5577','esrdty@outlook.com'),
(1,17,'4 Primaria','Alexa Grant','650-763-3949','fdgrt@outlook.com'),
(3,19,'2 Preparatoria','Ms. Alphonso Larkin','239-320-0741','.lk,jm@gmail.com'),
(2,18,'1 Secundaria','Sharon Zieme','633-353-3699','lkiujh@yahoo.com'),
(4,12,'5 Primaria','Al Ferry','264-458-5793','ghyj@yahoo.com'),
(5,17,'3 Secundaria','Adan Ferry','927-892-6411','gfvdc@outlook.com'),
(6,3,'1 Primaria','Camden Champlin','899-218-9387','hbty@outlook.com'),
(7,5,'2 Preparatoria','Corbin OHara','939-176-4313','gfvc@gmail.com'),
(1,7,'4 Primaria','Alba Howe','525-506-7053','@outlook.com'),
(2,7,'3 Secundaria','Niko','649-807-1964','@gmail.com'),
(3,8,'3 Primaria','Miss Gerard Tromp','758-505-6521','ouytc@hotmail..com'),
(15,20,'1 Secundaria','Magnus Goldner','123-679-1380','xzc vb@gmail.com'),
(17,9,'5 Primaria','Friedrich Feeney','858-884-2143','bnmjhkloi@outlook.com'),
(18,9,'2 Preparatoria','Monte Lindgren','631-634-8444','po90@outlook.com'),
(19,10,'3 Secundaria','Kevin','369-243-6989','dsfcgvh@outlook.com'),
(20,11,'2 Preparatoria','Royce Shanahan','002-205-3601','r6y7uj@hotmail..com'),
(20,19,'3 Preparatoria','Brandi Pfeffer','023-740-2226','56yt@yahoo.com'),
(20,7,'5 Primaria','Reagan Dickinson','187-352-6677','sa@outlook.com'),
(20,9,'1 Primaria','Laurence Doyle','890-081-5568','WVT@outlook.com'),
(2,8,'1 Secundaria','Ms. Lyric Abbott','844-008-3145','4VB5@outlook.com'),
(3,2,'4 Primaria','Demond Tremblay PhD','167-049-5240','NVK6@hotmail..com'),
(5,2,'2 Preparatoria','Savanah West','305-979-0404','TB4E@outlook.com'),
(7,2,'3 Primaria','Ashly Zboncak III','267-445-3927','BHY6@outlook.com'),
(8,1,'5 Primaria','Ransom Osinski','097-672-2868','CBDX@gmail.com'),
(9,3,'4 Primaria','Leo Rolfson Sr.','972-206-3006','AHTRE5@outlook.com'),
(11,5,'3 Primaria','Violette Hartmann','271-759-2283','B3YT@outlook.com'),
(10,6,'3 Secundaria','Matteo Goodwin','802-556-5085','VQER@gmail.com'),
(18,6,'1 Secundaria','Dovie Pagac','047-879-5405','ZTHRS@outlook.com'),
(14,8,'1 Primaria','Mr. Robbie Robel','009-244-1415','HTGJ5@yahoo.com'),
(13,8,'5 Primaria','Ola Hintz','286-199-5458','DSDE@gmail.com');



INSERT INTO`BeneficiariaCanalizador`(`idDeIngreso`,`IdCanalizador`)
VALUE
(1,2),
(2,1),
(3,3),
(5,5),
(7,10),
(8,12),
(10,19),
(11,18),
(13,11),
(15,10),
(5,9),
(10,8),
(19,7),
(18,3),
(17,2),
(16,1),
(15,6),
(7,10),
(14,11),
(13,12),
(11,15),
(1,17),
(3,19),
(2,18),
(4,12),
(5,17),
(6,3),
(7,5),
(1,7),
(2,7),
(3,8),
(15,20),
(17,9),
(18,9),
(19,10),
(20,11),
(20,1),
(20,7),
(20,9),
(2,8),
(3,2),
(5,2),
(7,2),
(8,1),
(11,5),
(10,6),
(18,6),
(14,8),
(13,8);

INSERT INTO`Usuario`(`Usuario`,`password`, `nombre`)
VALUE
('pumplow','sdnajs','Jesus Mendivil'),
('A058484','sdnajs','juan perez'),
('456456','sdnajs','Emilio acosta'),
('456456','sdnajs','Raul Galaviz'),
('787678','sdnajs','Jeesu morales'),
('74545678','sdnajs','Franco Escamilla'),
('43450','sdnajs','Robert Sandoval'),
('6786754','sdnajs','Cesar Larrinaga'),
('564046','sdnajs','Emily Corrier'),
('708797','sdnajs','Miles Prower'),
('0645064','sdnajs','Miles Fernandez'),
('046046','sdnajs','Inez Nunez'),
('0464','sdnajs','Anna Luz'),
('0464','sdnajs','Jose Henx'),
('asda','sdnajs','Katarina Dominguez'),
('1rqewew','sdnajs','Scott pilgrim'),
('a12qads','sdnajs','Yaveh Deux'),
('frasda','sdnajs','Deux Ex'),
('asdae','sdnajs','Asriel Dreemur'),
('asdr','sdnajs','Frisk Dreemur');

INSERT INTO `Rol`(`nombre`,`descripcion`)
VALUE
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Psicologo','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Director','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Administrador','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Medico','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.');

INSERT INTO `Privilegios`(`nombre`,`descripcion`)
VALUE
('Crear cuenta','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Borrar cuenta','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Registrar Beneficiaria','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Borrar Beneficiaria','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Modificar datos Beneficiaria','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Registrar Beneficiaria','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Subir fotos','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Crear album','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Borrar album','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Crear cuenta','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Crear cuenta','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Crear cuenta','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Crear cuenta','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Crear cuenta','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Crear cuenta','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Crear cuenta','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Crear cuenta','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Crear cuenta','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Crear cuenta','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.'),
('Crear cuenta','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet ac est sit amet gravida.');

INSERT INTO`RolPrivilegios`(`idRol`,`idPrivilegios`)
VALUE
(1,2),
(2,1),
(3,3),
(5,5),
(7,10),
(8,12),
(10,19),
(11,18),
(13,11),
(15,10),
(5,9),
(10,8),
(19,7),
(18,3),
(17,2),
(16,1),
(15,6),
(7,10),
(14,11),
(13,12),
(11,15),
(1,17),
(3,19),
(2,18),
(4,12),
(5,17),
(6,3),
(7,5),
(1,7),
(2,7),
(3,8),
(15,20),
(17,9),
(18,9),
(19,10),
(20,11),
(20,1),
(20,7),
(20,9),
(2,8),
(3,2),
(5,2),
(7,2),
(8,1),
(11,5),
(10,6),
(18,6),
(14,8),
(13,8);
INSERT INTO `UsuarioRol`(`idUser`,`idRol`, `created_at`)
VALUE

(1,2,'2019-3-15'),
(2,1,'2018-2-15'),
(3,3,'2017-1-15'),
(5,5,'2016-12-15'),
(7,10,'2015-08-15'),
(8,12,'2014-3-15'),
(10,19,'2012-4-15'),
(11,18,'2014-5-15'),
(13,11,'2015-6-15'),
(15,10,'2017-7-15'),
(5,9,'2018-8-15'),
(10,8,'2020-9-15'),
(19,7,'2020-10-15'),
(18,3,'2020-11-15'),
(17,2,'2020-12-15'),
(16,1,'2020-1-15'),
(15,6,'2020-2-15'),
(7,10,'2020-3-15'),
(14,11,'2020-4-15'),
(13,12,'2011-5-15'),
(11,15,'2018-6-15'),
(1,17,'2010-7-15'),
(3,19,'2015-8-15'),
(2,18,'2016-9-15'),
(4,12,'2017-1-15'),
(5,17,'2018-10-15'),
(6,3,'2019-11-15'),
(7,5,'2017-12-15'),
(1,7,'2015-1-15'),
(2,7,'2016-01-15'),
(3,8,'2014-3-15'),
(15,20,'2013-3-15'),
(17,9,'2012-3-15'),
(18,9,'2011-3-15'),
(19,10,'2010-5-15'),
(20,11,'2015-7-15'),
(20,19,'2001-8-15'),
(20,7,'2008-9-15'),
(20,9,'2007-10-15'),
(2,8,'2005-11-15'),
(3,2,'2006-12-15'),
(5,2,'2007-3-15'),
(7,2,'2008-2-15'),
(8,1,'2009-5-15'),
(9,3,'2010-7-15'),
(11,5,'2011-9-15'),
(10,6,'2012-7-15'),
(18,6,'2013-8-15'),
(14,8,'2015-6-15'),
(13,8,'2017-5-15');