<?php
    session_start();
    require_once("util.php");
    $id = htmlspecialchars($_GET["medicamento_id"]);
    $presentacion = htmlspecialchars($_GET["p"]);
    $_SESSION["medicamento_id"] = htmlspecialchars($_GET["medicamento_id"]);
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ModificarMedicamento/_modificarMedicamentoTitulo.html");
    echo editMedicamento($id);
    echo crear_select1("idPresentacion", "nombre", "presentacion",$presentacion);
    echo editMedicamento2($id);
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>