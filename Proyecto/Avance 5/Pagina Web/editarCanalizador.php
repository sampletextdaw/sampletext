<?php
    session_start();
    include("util.php");
    $id = htmlspecialchars($_GET["canalizador_id"]);
    $institucion = htmlspecialchars($_GET["i"]);
    $_SESSION["idC"] = $_GET["canalizador_id"];
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/EditarCanalizador/_editarCanalizadorTitulo.html"); 
    echo editCanalizador($id);
    echo crear_selectInstituciones("idInstitucion", "nombre", "institucion",$institucion);
    echo editCanalizador2($id);
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>