<?php
    session_start();
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultaMedicamento/_consultaMedicamentoTitulo.html");
        echo "<div class=\"row\">";
            echo "<div class=\"col s12\">";
            include("Partials/ConsultaMedicamento/_consultaMedicamentoHead.html");
            include("Partials/ConsultaMedicamento/_consultaMedicamento.html");    //cambio, para hacer nuestra tabla de consulta de Institucions dinamica debemos partir en 2 partials este archivo
            $nombre = "";

            showQueryMedicamentos(getMedicamentosPorNombre($nombre),$nombre);
            
            include("Partials/ConsultaMedicamento/_consultaMedicamentoFoot.html");
            echo "</div>";
        echo "</div>";
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>