<?php
    session_start();
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultaCanalizador/_consultaCanalizadorTitulo.html");
        echo "<div class=\"row\">";
            echo "<div class=\"col s12\">";
            include("Partials/ConsultaCanalizador/_consultaCanalizadorHead.html");
            include("Partials/ConsultaCanalizador/_consultaCanalizador.html");    //cambio, para hacer nuestra tabla de consulta de Institucions dinamica debemos partir en 2 partials este archivo
            $nombre = "";

            showQueryCanalizador(getCanalizadorPorNombre($nombre),$nombre);
            
            include("Partials/ConsultaCanalizador/_consultaCanalizadorFoot.html");
            echo "</div>";
        echo "</div>";
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>