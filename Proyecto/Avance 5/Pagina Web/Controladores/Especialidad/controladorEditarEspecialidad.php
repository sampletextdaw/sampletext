<?php
  session_start();
  require_once("../../util.php");  

  $_GET["especialidad_id"] = htmlspecialchars($_GET["especialidad_id"]);
  $_POST["especialidad_nombre"] = htmlspecialchars($_POST["especialidad_nombre"]);
  $_POST["area"] = htmlspecialchars($_POST["area"]);

  if(isset($_GET["especialidad_id"]) && isset($_POST["especialidad_nombre"])) {
      if (editarEspecialidad($_GET["especialidad_id"], $_POST["area"],$_POST["especialidad_nombre"],)) {
          $_SESSION["mensaje"] = "Se edito la especialidad";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al editar la especialidad";
      }
  }

  header("location:../../consultaEspecialidad.php");
?>