<?php
    session_start();
    require_once("../../util.php");
   
    if(isset($_POST['Registrar'])){
        $nombre = htmlspecialchars($_POST["nombre"]);
        $telefono = htmlspecialchars($_POST['telefono']);
        $email = htmlspecialchars($_POST['email']);
        $identificacion = htmlspecialchars($_POST['identificacion']);
        $numeroI = htmlspecialchars($_POST['numeroI']);
        $institucion = htmlspecialchars($_POST['institucion']);
        $cargo = htmlspecialchars($_POST['cargo']);
        $idC = htmlspecialchars($_SESSION["idC"]); 


        if (editarCanalizador($idC,$institucion,$nombre,$cargo,$telefono,$email,$identificacion,$numeroI)) {
            $_SESSION["mensaje"] = "Se ha actualizado el Canalizador";
            header("location:../../consultaCanalizador.php");
        } else {
            $_SESSION["warning"] = "Ocurrió un error al actualizado el Canalizador";
        }
    }
    
?>