<?php
  session_start();
  require_once("../../util.php");  

  $_GET["area_id"] = htmlspecialchars($_GET["area_id"]);
  $_POST["area_nombre"] = htmlspecialchars($_POST["area_nombre"]);

  if(isset($_GET["area_id"]) && isset($_POST["area_nombre"])) {
      if (editarArea($_GET["area_id"], $_POST["area_nombre"])) {
          $_SESSION["mensaje"] = "Se edito la area";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al editar la area";
      }
  }

  header("location:../../consultaArea.php");
?>