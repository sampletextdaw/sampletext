<?php
    session_start();
    require_once("../../util.php");

    if(isset($_POST['Actualizar'])){
        $id=$_SESSION["medicamento_id"];
        $nombre = htmlspecialchars($_POST["nombre"]);
        $ingrediente = htmlspecialchars($_POST['ingrediente']);
        $presentacion = htmlspecialchars($_POST['presentacion']);
        if (modificarMedicamento($id,$nombre,$ingrediente,$presentacion)) {
            $_SESSION["mensaje"] = "Se ha editado correctamente";
        } else {
            $_SESSION["warning"] = "Ocurrió un error al modificar";
        }
    }
    header("location:../../consultaMedicamento.php");
?>