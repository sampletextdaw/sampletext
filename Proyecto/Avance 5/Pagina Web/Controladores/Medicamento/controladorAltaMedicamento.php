<?php
    session_start();
    require_once("../../util.php");
   
    if(isset($_POST['Registrar'])){
        $nombre = htmlspecialchars($_POST["nombre"]);
        $ingrediente = htmlspecialchars($_POST['ingrediente']);
        $presentacion = htmlspecialchars($_POST['presentacion']);

        if (nuevoMedicamento($nombre,$ingrediente,$presentacion)) {
            $_SESSION["mensaje"] = "Se ha agregado el Medicamento";
            header("location:../../consultaMedicamento.php");
        } else {
            $_SESSION["warning"] = "Ocurrió un error al agregar el Medicamento";
        }
    }
    
?>