<?php
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
   
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/AltaCanalizador/_altaCanalizadorTitulo.html");
    include("Partials/AltaCanalizador/_altaCanalizadorFormulario.html");
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>